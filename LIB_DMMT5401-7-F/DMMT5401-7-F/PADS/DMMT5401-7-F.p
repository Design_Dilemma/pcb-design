*PADS-LIBRARY-PART-TYPES-V9*

DMMT5401-7-F SOT95P285X140-6N I ANA 9 1 0 0 0
TIMESTAMP 2021.06.04.03.18.11
"Manufacturer_Name" Diodes Inc.
"Manufacturer_Part_Number" DMMT5401-7-F
"Mouser Part Number" 621-DMMT5401-7-F
"Mouser Price/Stock" https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/DMMT5401-7-F?qs=hiZRsJxw0h3f7LzTPwvEFQ%3D%3D
"Arrow Part Number" DMMT5401-7-F
"Arrow Price/Stock" https://www.arrow.com/en/products/dmmt5401-7-f/diodes-incorporated
"Description" Transistor Dual PNP 150V 0.2A SOT26-6 Diodes Inc DMMT5401-7-F Dual PNP Bipolar Transistor, -200 mA, -150 V, 6-Pin SOT-26
"Datasheet Link" http://docs-emea.rs-online.com/webdocs/0e49/0900766b80e49504.pdf
"Geometry.Height" 1.4mm
GATE 1 6 0
DMMT5401-7-F
1 0 U C1
2 0 U B1
3 0 U B2
6 0 U E1
5 0 U E2
4 0 U C2

*END*
*REMARK* SamacSys ECAD Model
282220/519546/2.49/6/3/Integrated Circuit
