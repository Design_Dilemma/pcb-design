(part "DMMT5401-7-F"
    (packageRef "SOT95P285X140-6N")
    (interface
        (port "1" (symbPinId 1) (portName "C1") (portType INOUT))
        (port "2" (symbPinId 2) (portName "B1") (portType INOUT))
        (port "3" (symbPinId 3) (portName "B2") (portType INOUT))
        (port "4" (symbPinId 4) (portName "C2") (portType INOUT))
        (port "5" (symbPinId 5) (portName "E2") (portType INOUT))
        (port "6" (symbPinId 6) (portName "E1") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "101")
    (property "Manufacturer_Name" "Diodes Inc.")
    (property "Manufacturer_Part_Number" "DMMT5401-7-F")
    (property "Mouser_Part_Number" "621-DMMT5401-7-F")
    (property "Mouser_Price/Stock" "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/DMMT5401-7-F?qs=hiZRsJxw0h3f7LzTPwvEFQ%3D%3D")
    (property "Arrow_Part_Number" "DMMT5401-7-F")
    (property "Arrow_Price/Stock" "https://www.arrow.com/en/products/dmmt5401-7-f/diodes-incorporated")
    (property "Description" "Transistor Dual PNP 150V 0.2A SOT26-6 Diodes Inc DMMT5401-7-F Dual PNP Bipolar Transistor, -200 mA, -150 V, 6-Pin SOT-26")
    (property "Datasheet_Link" "http://docs-emea.rs-online.com/webdocs/0e49/0900766b80e49504.pdf")
    (property "symbolName1" "DMMT5401-7-F")
)
