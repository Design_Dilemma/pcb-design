EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7550 7500 0    50   ~ 0
EEE3088F Amplifier Submodule
Text Notes 10650 7600 0    50   ~ 0
1
Text Notes 8150 7650 0    50   ~ 0
1 June 2021
$Comp
L Amplifier_Operational:OP07 U1
U 1 1 60B635CB
P 5150 4050
F 0 "U1" H 5494 4004 50  0000 L CNN
F 1 "OP162" H 5494 4095 50  0000 L CNN
F 2 "Package_SON:Texas_S-PVSON-N10" H 5150 3650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa1622.pdf" H 5150 4050 50  0001 C CNN
	1    5150 4050
	1    0    0    1   
$EndComp
$Comp
L Device:R R7
U 1 1 60B66C0D
P 4250 3950
F 0 "R7" V 4043 3950 50  0000 C CNN
F 1 "15k" V 4134 3950 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 4180 3950 50  0001 C CNN
F 3 "~" H 4250 3950 50  0001 C CNN
	1    4250 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 4150 4700 4150
Wire Wire Line
	4700 4150 4700 4500
Text GLabel 3700 3950 0    50   Input ~ 0
Vin
Wire Wire Line
	4100 3950 3700 3950
$Comp
L Device:R R8
U 1 1 60B68F19
P 4900 3200
F 0 "R8" V 4693 3200 50  0000 C CNN
F 1 "10k" V 4784 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 4830 3200 50  0001 C CNN
F 3 "~" H 4900 3200 50  0001 C CNN
	1    4900 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 60B69904
P 5500 3200
F 0 "R9" V 5293 3200 50  0000 C CNN
F 1 "1k" V 5384 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 5430 3200 50  0001 C CNN
F 3 "~" H 5500 3200 50  0001 C CNN
	1    5500 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 60B6A0D2
P 6450 4050
F 0 "R10" V 6243 4050 50  0000 C CNN
F 1 "15k" V 6334 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 6380 4050 50  0001 C CNN
F 3 "~" H 6450 4050 50  0001 C CNN
	1    6450 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 3200 4600 3200
Wire Wire Line
	4600 3200 4600 3950
Wire Wire Line
	4400 3950 4600 3950
Connection ~ 4600 3950
Wire Wire Line
	4600 3950 4850 3950
Wire Wire Line
	5050 3200 5350 3200
Wire Wire Line
	5650 3200 5850 3200
Wire Wire Line
	5850 3200 5850 4050
Wire Wire Line
	5850 4050 5450 4050
Wire Wire Line
	5850 4050 6300 4050
Connection ~ 5850 4050
$Comp
L Amplifier_Operational:OP07 U2
U 1 1 60B6BBF9
P 7400 4150
F 0 "U2" H 7744 4104 50  0000 L CNN
F 1 "OP162" H 7744 4195 50  0000 L CNN
F 2 "Package_SON:Texas_S-PVSON-N10" H 7400 3750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa1622.pdf" H 7400 4150 50  0001 C CNN
	1    7400 4150
	1    0    0    1   
$EndComp
Wire Wire Line
	7100 4050 6950 4050
Text GLabel 8350 4150 2    50   Input ~ 0
Vout
Wire Wire Line
	7700 4150 8000 4150
Wire Wire Line
	7100 4250 7000 4250
Wire Wire Line
	7000 4250 7000 4600
$Comp
L Device:R R11
U 1 1 60B6F58F
P 7400 3300
F 0 "R11" V 7193 3300 50  0000 C CNN
F 1 "15k" V 7284 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 7330 3300 50  0001 C CNN
F 3 "~" H 7400 3300 50  0001 C CNN
	1    7400 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	7250 3300 6950 3300
Wire Wire Line
	6950 3300 6950 4050
Connection ~ 6950 4050
Wire Wire Line
	6950 4050 6600 4050
Wire Wire Line
	7550 3300 8000 3300
Wire Wire Line
	8000 3300 8000 4150
Connection ~ 8000 4150
Wire Wire Line
	8000 4150 8350 4150
NoConn ~ 5150 3750
NoConn ~ 5250 3750
NoConn ~ 7400 3850
NoConn ~ 7500 3850
Text GLabel 5050 3600 1    50   Input ~ 0
V-
Text GLabel 7300 3700 1    50   Input ~ 0
V-
Wire Wire Line
	7300 3700 7300 3850
Wire Wire Line
	5050 3600 5050 3750
Text GLabel 5050 4500 3    50   Input ~ 0
V+
Text GLabel 7300 4600 3    50   Input ~ 0
V+
Wire Wire Line
	7300 4450 7300 4600
Wire Wire Line
	5050 4350 5050 4500
Text GLabel 4700 4500 0    50   Input ~ 0
GND2
Text GLabel 7000 4600 0    50   Input ~ 0
GND2
$EndSCHEMATC
