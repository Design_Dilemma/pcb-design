EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7450 7500 0    50   ~ 0
Switching Voltage Regulator
Text Notes 8150 7650 0    50   ~ 0
June 2021
$Comp
L Device:R R7
U 1 1 60B91610
P 4550 2350
F 0 "R7" H 4620 2396 50  0000 L CNN
F 1 "11053" H 4620 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 4480 2350 50  0001 C CNN
F 3 "~" H 4550 2350 50  0001 C CNN
	1    4550 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 60B92189
P 4550 2650
F 0 "R8" H 4620 2696 50  0000 L CNN
F 1 "10k" H 4620 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 4480 2650 50  0001 C CNN
F 3 "~" H 4550 2650 50  0001 C CNN
	1    4550 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 60B92281
P 4550 3350
F 0 "R9" H 4620 3396 50  0000 L CNN
F 1 "11053" H 4620 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 4480 3350 50  0001 C CNN
F 3 "~" H 4550 3350 50  0001 C CNN
	1    4550 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 60B922DC
P 4550 3650
F 0 "R10" H 4620 3696 50  0000 L CNN
F 1 "10k" H 4620 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 4480 3650 50  0001 C CNN
F 3 "~" H 4550 3650 50  0001 C CNN
	1    4550 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 60B95C91
P 4550 2800
F 0 "#PWR01" H 4550 2550 50  0001 C CNN
F 1 "GND" H 4555 2627 50  0000 C CNN
F 2 "" H 4550 2800 50  0001 C CNN
F 3 "" H 4550 2800 50  0001 C CNN
	1    4550 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 60B96B1B
P 4550 3800
F 0 "#PWR02" H 4550 3550 50  0001 C CNN
F 1 "GND" H 4555 3627 50  0000 C CNN
F 2 "" H 4550 3800 50  0001 C CNN
F 3 "" H 4550 3800 50  0001 C CNN
	1    4550 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2500 5150 2500
Wire Wire Line
	5150 2500 5150 2900
Connection ~ 4550 2500
Wire Wire Line
	4850 2900 4850 2950
Wire Wire Line
	5450 2900 5450 2950
Wire Wire Line
	5150 3050 5150 3500
Wire Wire Line
	5150 3500 4550 3500
Connection ~ 4550 3500
Wire Wire Line
	5550 2950 5450 2950
Connection ~ 5450 2950
Wire Wire Line
	5450 2950 5450 3050
Wire Wire Line
	4800 2950 4850 2950
Connection ~ 4850 2950
Wire Wire Line
	4850 2950 4850 3050
$Comp
L power:GND #PWR03
U 1 1 60BA0C6A
P 7300 3950
F 0 "#PWR03" H 7300 3700 50  0001 C CNN
F 1 "GND" H 7305 3777 50  0000 C CNN
F 2 "" H 7300 3950 50  0001 C CNN
F 3 "" H 7300 3950 50  0001 C CNN
	1    7300 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 60BA18CC
P 8450 3450
F 0 "L1" V 8640 3450 50  0000 C CNN
F 1 "0.33u" V 8549 3450 50  0000 C CNN
F 2 "Inductor_SMD:L_6.3x6.3_H3" H 8450 3450 50  0001 C CNN
F 3 "~" H 8450 3450 50  0001 C CNN
	1    8450 3450
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D9
U 1 1 60BA2051
P 8150 3600
F 0 "D9" V 8150 3450 50  0000 C CNN
F 1 "1N4148" V 8250 3450 50  0000 C CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8150 3425 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 8150 3600 50  0001 C CNN
	1    8150 3600
	0    1    1    0   
$EndComp
Text GLabel 4550 2200 0    50   Input ~ 0
Vin+
Text GLabel 4550 3200 0    50   Input ~ 0
Vin-
$Comp
L Device:R R13
U 1 1 60BAA48B
P 8850 3600
F 0 "R13" H 8920 3646 50  0000 L CNN
F 1 "10k" H 8920 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 8780 3600 50  0001 C CNN
F 3 "~" H 8850 3600 50  0001 C CNN
	1    8850 3600
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:LM2672M-5.0 U2
U 1 1 60BAB56B
P 7300 3200
F 0 "U2" H 7300 3667 50  0000 C CNN
F 1 "LM2672M-5.0" H 7300 3576 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7350 2850 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm2672.pdf" H 7300 3200 50  0001 C CNN
	1    7300 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 3700 7300 3600
Wire Wire Line
	7800 3400 7800 3450
Wire Wire Line
	7800 3200 7800 3150
Wire Wire Line
	7800 3150 7950 3150
Wire Wire Line
	8150 3450 8300 3450
Connection ~ 8150 3450
Wire Wire Line
	8600 3450 8600 3650
Wire Wire Line
	8600 3450 8850 3450
Connection ~ 8600 3450
Wire Wire Line
	8850 3750 8850 3950
Wire Wire Line
	7300 3950 7300 3700
Connection ~ 7300 3950
Connection ~ 7300 3700
Wire Wire Line
	8600 3450 8600 3000
Wire Wire Line
	8600 3000 7800 3000
Text GLabel 8850 3450 2    50   Input ~ 0
Vout
$Comp
L Driver_FET:MCP1416 U1
U 1 1 60BB85A1
P 6200 3300
F 0 "U1" H 6350 3700 50  0000 L CNN
F 1 "MCP1416" H 6350 3650 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6200 2900 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20002092F.pdf" H 6000 3550 50  0001 C CNN
	1    6200 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 60BC1F64
P 5750 3300
F 0 "R12" V 5957 3300 50  0000 C CNN
F 1 "1k" V 5866 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 5680 3300 50  0001 C CNN
F 3 "~" H 5750 3300 50  0001 C CNN
	1    5750 3300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R11
U 1 1 60BC22DE
P 5750 3000
F 0 "R11" V 5957 3000 50  0000 C CNN
F 1 "1k" V 5866 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 5680 3000 50  0001 C CNN
F 3 "~" H 5750 3000 50  0001 C CNN
	1    5750 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP C1
U 1 1 60BC35E2
P 5550 3500
F 0 "C1" H 5668 3546 50  0000 L CNN
F 1 "0.33u" H 5668 3455 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 5588 3350 50  0001 C CNN
F 3 "~" H 5550 3500 50  0001 C CNN
	1    5550 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 60BC3C84
P 7950 3300
F 0 "C2" H 8068 3346 50  0000 L CNN
F 1 "100n" H 8068 3255 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 7988 3150 50  0001 C CNN
F 3 "~" H 7950 3300 50  0001 C CNN
	1    7950 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C3
U 1 1 60BC3FB5
P 8600 3800
F 0 "C3" H 8718 3846 50  0000 L CNN
F 1 "0.33u" H 8718 3755 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 8638 3650 50  0001 C CNN
F 3 "~" H 8600 3800 50  0001 C CNN
	1    8600 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 3450 7950 3450
Connection ~ 7950 3450
Wire Wire Line
	7950 3450 8150 3450
Wire Wire Line
	7300 3950 8150 3950
Connection ~ 8600 3950
Wire Wire Line
	8600 3950 8850 3950
Connection ~ 5550 2950
Wire Wire Line
	5900 3000 6200 3000
Wire Wire Line
	5550 3700 5550 3650
Wire Wire Line
	4800 3700 5550 3700
Wire Wire Line
	4800 2950 4800 3700
Connection ~ 5550 3700
Wire Wire Line
	5550 2950 5550 3000
Wire Wire Line
	5600 3300 5550 3300
Connection ~ 5550 3300
Wire Wire Line
	5550 3300 5550 3350
Wire Wire Line
	5600 3000 5550 3000
Connection ~ 5550 3000
Wire Wire Line
	5550 3000 5550 3300
Wire Wire Line
	6800 3400 6600 3400
Wire Wire Line
	6600 3400 6600 3300
Wire Wire Line
	6800 2950 6800 3000
Wire Wire Line
	5550 2950 6800 2950
Wire Wire Line
	8150 3750 8150 3950
Connection ~ 8150 3950
Wire Wire Line
	8150 3950 8600 3950
Wire Wire Line
	5550 3700 6200 3700
Wire Wire Line
	6200 3600 6200 3700
Connection ~ 6200 3700
Wire Wire Line
	6200 3700 7300 3700
$Comp
L Diode:1N5711 D5
U 1 1 60BEC1B8
P 5000 2900
F 0 "D5" H 5000 3117 50  0000 C CNN
F 1 "1N5711" H 5000 3026 50  0000 C CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5000 2725 50  0001 C CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8865-lds-0040-datasheet" H 5000 2900 50  0001 C CNN
	1    5000 2900
	-1   0    0    -1  
$EndComp
$Comp
L Diode:1N5711 D7
U 1 1 60BEE8BB
P 5300 2900
F 0 "D7" H 5300 3117 50  0000 C CNN
F 1 "1N5711" H 5300 3026 50  0000 C CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5300 2725 50  0001 C CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8865-lds-0040-datasheet" H 5300 2900 50  0001 C CNN
	1    5300 2900
	-1   0    0    -1  
$EndComp
$Comp
L Diode:1N5711 D6
U 1 1 60BEF3B1
P 5000 3050
F 0 "D6" H 5000 2900 50  0000 C CNN
F 1 "1N5711" H 5000 2850 50  0000 C CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5000 2875 50  0001 C CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8865-lds-0040-datasheet" H 5000 3050 50  0001 C CNN
	1    5000 3050
	-1   0    0    -1  
$EndComp
$Comp
L Diode:1N5711 D8
U 1 1 60BEFB7B
P 5300 3050
F 0 "D8" H 5300 2900 50  0000 C CNN
F 1 "1N5711" H 5300 2850 50  0000 C CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5300 2875 50  0001 C CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8865-lds-0040-datasheet" H 5300 3050 50  0001 C CNN
	1    5300 3050
	-1   0    0    -1  
$EndComp
Connection ~ 5150 2900
Connection ~ 5150 3050
$EndSCHEMATC
