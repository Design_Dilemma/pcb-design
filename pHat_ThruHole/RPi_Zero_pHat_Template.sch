EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 2300 4550 0    100  Italic 0
Thru-Hole Connector
$Sheet
S 5650 1300 1350 1100
U 60B9A1AD
F0 "Status LEDs" 50
F1 "Status LEDs.sch" 50
$EndSheet
$Sheet
S 8000 1300 1400 1150
U 60B9A27F
F0 "ZVD Circuit" 50
F1 "ZVD Circuit.sch" 50
$EndSheet
$Sheet
S 5650 3050 1350 1150
U 60B9A36F
F0 "Voltage Regulator" 50
F1 "Voltage Regulator.sch" 50
$EndSheet
$Sheet
S 8050 3050 1400 1150
U 60B9A41F
F0 "Amplifier" 50
F1 "Amplifier.sch" 50
$EndSheet
Text GLabel 4050 2450 2    50   Input ~ 0
GND1
Text GLabel 2150 2650 0    50   Input ~ 0
GND2
Text GLabel 2150 4050 0    50   Input ~ 0
GPIO26
Text GLabel 2150 3750 0    50   Input ~ 0
GPIO6
Text GLabel 4050 3950 2    50   Input ~ 0
GPIO16
Text GLabel 4050 2250 2    50   Input ~ 0
GPIO2
Text GLabel 2150 4150 0    50   Input ~ 0
GND8
Text GLabel 4050 3850 2    50   Input ~ 0
GND7
Text GLabel 2150 2750 0    50   Input ~ 0
GPIO17
$Comp
L Connector-ML:RPi_GPIO J1
U 1 1 5516AE26
P 2350 2250
F 0 "J1" H 3100 2500 60  0000 C CNN
F 1 "RPi_GPIO" H 3100 2400 60  0000 C CNN
F 2 "RPi_Hat:Pin_Header_Straight_2x20" H 2350 2250 60  0001 C CNN
F 3 "" H 2350 2250 60  0000 C CNN
	1    2350 2250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
