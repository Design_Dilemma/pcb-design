The uHAT will be attached to a Raspberry Pi Zero board and use the available hardware on this device.

An analog input signal from a water pressure sensor is taken and using the relationship between pressure and depth underwater, the device will sense, record and display its depth up to 150m below the water’s surface.

Using Raspberry’s timing module, the device will take and store distance measurements at set time intervals. 

These recordings will be stored within an EEPROM/microSD card to be read on a computer. 

For on-site reading, there will be a small LCD screen as well as LEDs that light up and remain lit upon reaching certain depths. Thus, when the device is retrieved, the LEDs can be read to indicate a range of depth.
