EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:R R5
U 1 1 60BA6FF7
P 6050 5250
F 0 "R5" H 6118 5296 50  0000 L CNN
F 1 "47k" H 6118 5205 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" H 6050 5250 50  0001 C CNN
F 3 "~" H 6050 5250 50  0001 C CNN
	1    6050 5250
	1    0    0    -1  
$EndComp
$Comp
L pspice:R R6
U 1 1 60BA76F7
P 6650 5250
F 0 "R6" H 6718 5296 50  0000 L CNN
F 1 "47k" H 6718 5205 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" H 6650 5250 50  0001 C CNN
F 3 "~" H 6650 5250 50  0001 C CNN
	1    6650 5250
	1    0    0    -1  
$EndComp
$Comp
L DMG2305UXQ-7:DMG2305UXQ-7 Q?
U 1 1 60BA835E
P 6150 2700
AR Path="/60BA835E" Ref="Q?"  Part="1" 
AR Path="/60B9A27F/60BA835E" Ref="Q1"  Part="1" 
F 0 "Q1" V 6717 2800 50  0000 C CNN
F 1 "DMG2305UXQ-7" V 6626 2800 50  0000 C CNN
F 2 "DMG2305UXQ-7:SOT96P240X110-3N" H 6600 2650 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/DMG2305UXQ-7.pdf" H 6600 2550 50  0001 L CNN
F 4 "MOSFET MOSFET BVDSS" H 6600 2450 50  0001 L CNN "Description"
F 5 "1.1" H 6600 2350 50  0001 L CNN "Height"
F 6 "Diodes Inc." H 6600 2250 50  0001 L CNN "Manufacturer_Name"
F 7 "DMG2305UXQ-7" H 6600 2150 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "621-DMG2305UXQ-7" H 6600 2050 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/DMG2305UXQ-7?qs=60RJRzIpcl%2FUiAojHt1c9w%3D%3D" H 6600 1950 50  0001 L CNN "Mouser Price/Stock"
F 10 "DMG2305UXQ-7" H 6600 1850 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/dmg2305uxq-7/diodes-incorporated" H 6600 1750 50  0001 L CNN "Arrow Price/Stock"
	1    6150 2700
	0    -1   -1   0   
$EndComp
$Comp
L DMMT5401-7-F:DMMT5401-7-F IC1
U 1 1 60BA5967
P 6050 4550
F 0 "IC1" H 6500 4815 50  0000 C CNN
F 1 "DMMT5401-7-F" H 6500 4724 50  0000 C CNN
F 2 "DMMT5401-7-F:SOT95P285X140-6N" H 6800 4650 50  0001 L CNN
F 3 "http://docs-emea.rs-online.com/webdocs/0e49/0900766b80e49504.pdf" H 6800 4550 50  0001 L CNN
F 4 "Transistor Dual PNP 150V 0.2A SOT26-6 Diodes Inc DMMT5401-7-F Dual PNP Bipolar Transistor, -200 mA, -150 V, 6-Pin SOT-26" H 6800 4450 50  0001 L CNN "Description"
F 5 "1.4" H 6800 4350 50  0001 L CNN "Height"
F 6 "Diodes Inc." H 6800 4250 50  0001 L CNN "Manufacturer_Name"
F 7 "DMMT5401-7-F" H 6800 4150 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "621-DMMT5401-7-F" H 6800 4050 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/DMMT5401-7-F?qs=hiZRsJxw0h3f7LzTPwvEFQ%3D%3D" H 6800 3950 50  0001 L CNN "Mouser Price/Stock"
F 10 "DMMT5401-7-F" H 6800 3850 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/dmmt5401-7-f/diodes-incorporated" H 6800 3750 50  0001 L CNN "Arrow Price/Stock"
	1    6050 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 3650 6650 3650
Wire Wire Line
	6150 3450 6350 3450
Wire Wire Line
	6350 3450 6350 2850
Wire Wire Line
	6050 3650 5750 3650
Text GLabel 7600 2850 2    50   Input ~ 0
GPIO2
Wire Wire Line
	7600 2850 6350 2850
Connection ~ 6350 2850
Wire Wire Line
	6350 2850 6350 2400
Wire Wire Line
	5750 3650 5750 2850
Wire Wire Line
	5750 2850 4550 2850
Connection ~ 5750 2850
Wire Wire Line
	5750 2850 5750 2400
Wire Wire Line
	6050 4550 6050 4900
Wire Wire Line
	6650 3650 6650 4200
Wire Wire Line
	6150 3650 6150 3450
Wire Wire Line
	6150 2700 6150 3250
Wire Wire Line
	6150 3250 6950 3250
Wire Wire Line
	6950 3250 6950 4200
Wire Wire Line
	6950 4200 6650 4200
Connection ~ 6650 4200
Wire Wire Line
	6650 4200 6650 5000
Wire Wire Line
	6250 4550 6250 4900
Wire Wire Line
	6250 4900 6150 4900
Connection ~ 6050 4900
Wire Wire Line
	6050 4900 6050 5000
Wire Wire Line
	6150 4550 6150 4900
Connection ~ 6150 4900
Wire Wire Line
	6150 4900 6050 4900
Text GLabel 6350 6000 3    50   Input ~ 0
GND
Wire Wire Line
	6050 5500 6050 6000
Wire Wire Line
	6650 5500 6650 6000
Wire Wire Line
	6050 6000 6650 6000
Text GLabel 4550 2850 0    50   Input ~ 0
VZVD
$EndSCHEMATC
